# [🍔 Foodies](https://foodies-vue.netlify.app/) 

## Project setup

### Clone repository
```
git clone git@gitlab.com:fmiguelop/foodies.git
```

### Install dependencies

```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev
```

### Compiles and minifies for production
```
npm run build
```

## Deployment Status
[![Netlify Status](https://api.netlify.com/api/v1/badges/ac4edb6f-2b50-4654-848c-d84394af4105/deploy-status)](https://app.netlify.com/sites/foodies-vue/deploys)

---

## Notes
- Hosting site service blocks HTTP request making Druk Font not display. 
- API images for menu items not available.


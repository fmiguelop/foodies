module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        'druk': ['"Druk Text Wide Web Medium"', 'sans-serif'],
        'syne': ['Syne', 'sans-serif'],
      },
      spacing: {
        '108': '37rem',
        '0.75': '0.75rem',
        '94' : '20.5rem',
        '100': '27.25rem',
        '95': '23.25rem',
        '102': '32rem',
        '103' : '34.5rem',
        '190': '46rem',
        '200': '50rem',
      },
      backgroundImage: {
        'spirals' : "url('src/assets/images/spirals.svg')",
      }
    },
    zIndex:{
      '0': -1,
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}

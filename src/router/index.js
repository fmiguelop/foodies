import { createRouter, createWebHistory } from 'vue-router'
import LandingPage from '../views/LandingPage.vue'
import Menu from '../views/Menu.vue'

const routes = [
    {
        path: '/',
        name: 'Home',
        component: LandingPage,
    },
    {
        path: '/menu',
        name: 'Menu',
        component: Menu,
    }
]
const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router